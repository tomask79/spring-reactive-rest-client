package com.example;

import com.example.domain.Item;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Collections;

@SpringBootApplication
public class DemoApplication {

	@Bean
	public WebClient webClient() {
		return WebClient.create("http://localhost:8081");
	}

	@Bean
	CommandLineRunner launch(WebClient webClient) {
		return args -> {
			webClient.get().uri("/store/1059304054")
					.accept(MediaType.TEXT_EVENT_STREAM)
					.exchange()
					.flatMap(cr -> cr.bodyToFlux(Item.class))
					.subscribe(v -> {
						System.out.println("Received from MS: "+v.getName());
					});
		};
	}

	public static void main(String args[]) {
		new SpringApplicationBuilder(DemoApplication.class).properties
				(Collections.singletonMap("server.port", "8082"))
				.run(args);
	}
}
